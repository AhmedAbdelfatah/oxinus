import * as bcrypt from 'bcrypt';
const saltOrRounds = 10;
const encrypt = async (data): Promise<string> => {
  return await bcrypt.hash(data, saltOrRounds);
};
export { encrypt };
