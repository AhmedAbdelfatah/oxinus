import { IsDate, MaxLength } from 'class-validator';
import { CreateUserInput } from './create-user.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @Field(() => Int)
  id: number;

  @Field(() => String, { description: 'User first name', nullable: true })
  @MaxLength(100)
  firstName?: string;

  @Field(() => String, { description: 'User last name', nullable: true })
  @MaxLength(100)
  lastName?: string;

  @Field(() => String, { description: 'User email', nullable: true })
  @MaxLength(100)
  email?: string;

  @Field(() => String, { description: 'User phone', nullable: true })
  @MaxLength(16)
  phone?: string;

  @Field(() => String, { description: 'User password', nullable: true })
  @MaxLength(50)
  password?: string;

  @Field(() => Date, { description: 'User date of birth', nullable: true })
  @IsDate()
  dateOfBirth?: Date;
}
