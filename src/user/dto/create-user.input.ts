import { InputType, Field, HideField } from '@nestjs/graphql';
import { IsDate, IsNotEmpty, MaxLength } from 'class-validator';

@InputType()
export class CreateUserInput {
  @Field(() => String, { description: 'User first name' })
  @IsNotEmpty()
  @MaxLength(100)
  firstName: string;

  @Field(() => String, { description: 'User last name' })
  @IsNotEmpty()
  @MaxLength(100)
  lastName: string;

  @Field(() => String, { description: 'User email' })
  @IsNotEmpty()
  @MaxLength(100)
  email: string;

  @Field(() => String, { description: 'User phone' })
  @MaxLength(16)
  phone: string;

  @Field(() => String, { description: 'User password' })
  @IsNotEmpty()
  @MaxLength(50)
  @HideField()
  password: string;

  @Field(() => Date, { description: 'User date of birth' })
  @IsNotEmpty()
  @IsDate()
  dateOfBirth: Date;
}
