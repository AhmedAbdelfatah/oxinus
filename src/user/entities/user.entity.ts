import { ObjectType, Field, Int, HideField } from '@nestjs/graphql';

@ObjectType()
export class User {
  @Field(() => Int, { description: 'User Id' })
  id: number;

  @Field(() => String, { description: 'User first name' })
  firstName: string;

  @Field(() => String, { description: 'User last name' })
  lastName: string;

  @Field(() => String, { description: 'User email' })
  email: string;

  @Field(() => String, { description: 'User phone' })
  phone: string;

  @Field(() => String, { description: 'User password' })
  @HideField()
  password: string;

  @Field(() => Date, { description: 'User date of birth' })
  dateOfBirth: Date;
}
