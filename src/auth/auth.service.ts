import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FirebaseService } from 'src/firebase/firebase.service';
import {
  AuthError,
  signInWithEmailAndPassword,
  UserCredential,
} from 'firebase/auth';

@Injectable()
export class AuthService {
  constructor(private firebaseService: FirebaseService) {}

  public async login({ email, password }) {
    try {
      const userCredential: UserCredential = await signInWithEmailAndPassword(
        this.firebaseService.auth,
        email,
        password,
      );
      return userCredential.user.toJSON();
    } catch (error: unknown) {
      const firebaseAuthError = error as AuthError;
      console.log(`[FIREBASE AUTH ERROR CODE]: ${firebaseAuthError.code}`);

      if (firebaseAuthError.code === 'auth/wrong-password') {
        throw new HttpException(
          'Email or password incorrect.',
          HttpStatus.FORBIDDEN,
        );
      }

      if (firebaseAuthError.code === 'auth/user-not-found') {
        throw new HttpException('Email not found.', HttpStatus.NOT_FOUND);
      }
    }
  }
}
