import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { ConfigModule } from '@nestjs/config';
import { FirebaseService } from 'src/firebase/firebase.service';

@Module({
  imports: [ConfigModule],
  providers: [AuthResolver, AuthService, FirebaseService],
})
export class AuthModule {}
