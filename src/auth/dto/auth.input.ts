import { InputType, Field } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class AuthInput {
  @Field(() => String, { description: 'Oauth firebase user email' })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field(() => String, { description: 'Oauth firebase user password' })
  @IsNotEmpty()
  password: string;
}
