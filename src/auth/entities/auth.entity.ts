import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class Auth {
  @Field(() => String, { description: 'Access token value' })
  token: string;
  @Field(() => String, { description: 'Firebase loggedin email' })
  email: string;
}
