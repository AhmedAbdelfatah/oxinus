import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { AuthInput } from './dto/auth.input';
import 'firebase/auth';
import { Auth } from './entities/auth.entity';

@Resolver()
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => Auth)
  async login(@Args('authInput') authInput: AuthInput) {
    try {
      const user = await this.authService.login(authInput);
      return {
        token: user['stsTokenManager'].accessToken,
        email: user['email'],
      };
    } catch (error) {
      throw new Error(error.message);
    }
  }
}
